package com.mzr

import kotlin.test.Test
import kotlin.test.assertEquals


class RunnerTest {

    private val testRunner: Runner = Runner()

    @Test
    fun testPrint() {
        assertEquals("It works!", testRunner.print())
    }

}