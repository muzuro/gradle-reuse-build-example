package com.mzr

class Runner {

    fun print(): String {
        return "It works!"
    }

}

fun main() {
    print(Runner().print())
}
